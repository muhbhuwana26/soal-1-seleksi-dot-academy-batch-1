<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

	public function tambah()
	{
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'nama' => $this->input->post('nama')
		);

		$username = $this->input->post('username');
		$sql = $this->db->query("SELECT username FROM user where username='$username'");
		$cek_username = $sql->num_rows();
		if ($cek_username > 0) {
			$this->session->set_flashdata('notif', 'Username Already Exist');
			redirect(site_url('register/index'));
		} else {
		$this->db->insert('user',$data);
		}
		if($this->db->affected_rows()>0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file Register_model.php */
/* Location: ./application/models/Register_model.php */