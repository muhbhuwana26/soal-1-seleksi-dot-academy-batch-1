<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function get_user(){
		return $this->db->get('user')
						->result();
	}

	public function tambah()
	{
		$data = array(
				'nama' 		=> $this->input->post('nama'),
				'username'	=> $this->input->post('username'),
				'password'	=> $this->input->post('password'),
			);

		$this->db->insert('user', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function ubah()
	{
		$data = array(
				'nama' 		=> $this->input->post('ubah_nama'),
				'username'	=> $this->input->post('ubah_username'),
				'password'	=> $this->input->post('ubah_password'),
			);

		$this->db->where('id_user', $this->input->post('ubah_id_user'))
				 ->update('user', $data);
		
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function get_data_user_by_id($id)
	{
		return $this->db->where('id_user', $id)
						->get('user')
						->row();
	}
	public function hapus()
	{
		$this->db->where('id_user', $this->input->post('hapus_id_user'))
				 ->delete('user');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */