<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_model extends CI_Model {

	public function get_buku(){
		return $this->db->get('data_buku')
						->result();
	}
	public function tambah()
	{
		$data = array(
				'judul_buku' 	=> $this->input->post('buku'),
				'penerbit'		=> $this->input->post('penerbit'),
				'tahun'			=> $this->input->post('tahun'),
				'stok'			=> $this->input->post('stok'),
				'harga'			=> $this->input->post('harga'),
			);

		$this->db->insert('data_buku', $data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function get_data_buku_by_id($id)
	{
		return $this->db->where('id_buku', $id)
						->get('data_buku')
						->row();
	}
	public function ubah()
	{
		$data = array(
				'id_buku' 		=> $this->input->post('ubah_kode_buku'),
				'judul_buku' 	=> $this->input->post('ubah_buku'),
				'penerbit'	=> $this->input->post('ubah_penerbit'),
				'tahun'		=> $this->input->post('ubah_tahun'),
				'stok'			=> $this->input->post('ubah_stok'),
				'harga'			=> $this->input->post('ubah_harga')
			);

		$this->db->where('id_buku', $this->input->post('ubah_id_buku'))
				 ->update('data_buku', $data);
		
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function hapus()
	{
		$this->db->where('id_buku', $this->input->post('hapus_id_buku'))
				 ->delete('data_buku');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	

}

/* End of file Buku_model.php */
/* Location: ./application/models/Buku_model.php */