<div class="row">
	<div class="col-md-6">
		<h3>
			<i class="fa fa-angle-right"></i>
		Daftar Menu
		</h3>
	</div>
	<div class="col-md-6">
		<ol class="breadcrumb float-md-right">
			<button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#tambah">Tambah menu</button>
         </ol>
	</div>
	<div class="col-md-12">
			<?php
			$notif = $this->session->flashdata('notif');
			if($notif != NULL){
				echo '
					<div class="alert alert-danger">'.$notif.'</div>
				';
			}
		?>
	</div>
	<div class="col-md-12">
<!-- TABLE STRIPED -->
		<table class="table table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Id</th>
					<th>Judul Buku</th>
					<th>Penerbit</th>
					<th>Tahun</th>
					<th>Harga</th>
					<th>Stok</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			<?php
				$no = 1;
				foreach ($buku as $b) {
					echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$b->id_buku.'</td>
							<td>'.$b->judul_buku.'</td>
							<td>'.$b->penerbit.'</td>
							<td>'.$b->tahun.'</td>
							<td>Rp '.$b->harga.',-</td>
							<td>'.$b->stok.'</td>
							<td>
								<a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ubah" onclick="prepare_ubah_buku('.$b->id_buku.')">Ubah</a>
								<a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus" onclick="prepare_hapus_buku('.$b->id_buku.')">hapus</a>
							</td>
						</tr>
					';
					$no++;
				}
			?>
				
			</tbody>
		</table>
<!-- END TABLE STRIPED -->
	</div>
</div>
<!-- Modal -->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Buku</h4>
      </div>
      <form action="<?php echo base_url('index.php/buku/tambah'); ?>" method="post" enctype="multipart/form-data">
	      <div class="modal-body">
	        	<input type="text" class="form-control" placeholder="Judul Buku" name="buku">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Penerbit" name="penerbit">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Tahun" name="tahun">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Harga" name="harga">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Stok" name="stok">

	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" name="submit" value="SIMPAN">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      </form>
    </div>
  </div>
</div>
<div id="ubah" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah menu</h4>
      </div>
      <form action="<?php echo base_url('index.php/buku/ubah'); ?>" method="post" enctype="multipart/form-data">
	      <div class="modal-body">
	        	<input type="hidden" name="ubah_id_buku"  id="ubah_id_buku">
	        	<input type="text" class="form-control" placeholder="Id Buku" name="ubah_kode_buku"  id="ubah_kode_buku">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Judul Buku" name="ubah_buku"  id="ubah_buku">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Penerbit" name="ubah_penerbit"  id="ubah_penerbit">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Tahun" name="ubah_tahun" id="ubah_tahun">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Harga" name="ubah_harga" id="ubah_harga">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Stok" name="ubah_stok" id="ubah_stok">
	        	<br>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" name="submit" value="SIMPAN">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      </form>
    </div>
  </div>
</div>
<div id="hapus" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi Hapus Data Buku</h4>
      </div>
      <form action="<?php echo base_url('index.php/buku/hapus'); ?>" method="post">
	      <div class="modal-body">
	        	<input type="hidden" name="hapus_id_buku"  id="hapus_id_buku">
	        	<p>Apakah anda yakin menghapus data buku <b><span id="hapus_judul"></span></b> ?</p>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-danger" name="submit" value="YA">
	        <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	
	function prepare_ubah_buku(id)
	{
		$("#ubah_id_buku").empty();
		$("#ubah_kode_buku").empty();
		$("#ubah_buku").empty();;
		$("#ubah_penerbit").val();
		$("#ubah_tahun").empty();
		$("#ubah_harga").empty();
		$("#ubah_stok").empty();
		

		$.getJSON('<?php echo base_url(); ?>index.php/buku/get_data_buku_by_id/' + id,  function(data){
			$("#ubah_id_buku").val(data.id_buku);
			$("#ubah_kode_buku").val(data.id_buku);
			$("#ubah_buku").val(data.judul_buku);
			$("#ubah_penerbit").val(data.penerbit);
			$("#ubah_tahun").val(data.tahun);
			$("#ubah_harga").val(data.harga);
			$("#ubah_stok").val(data.stok);
			

		});
	}

	function prepare_hapus_buku(id)
	{
		$("#hapus_id_buku").empty();
		$("#hapus_judul").empty();

		$.getJSON('<?php echo base_url(); ?>index.php/buku/get_data_buku_by_id/' + id,  function(data){
			$("#hapus_id_buku").val(data.id_buku);
			$("#hapus_judul").text(data.judul_buku);
		});
	}
</script>