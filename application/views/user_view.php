<div class="row">
	<div class="col-md-6">
		<h3>
			<i class="fa fa-angle-right"></i>
		Daftar User
		</h3>
	</div>
	<div class="col-md-6">
		<ol class="breadcrumb float-md-right">
			<button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#tambah">Tambah User</button>
         </ol>
	</div>
	<div class="col-md-12">
			<?php
			$notif = $this->session->flashdata('notif');
			if($notif != NULL){
				echo '
					<div class="alert alert-danger">'.$notif.'</div>
				';
			}
		?>
	</div>
	<div class="col-md-12">
<!-- TABLE STRIPED -->
		<table class="table table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th>Id</th>
					<th>Nama</th>
					<th>Username</th>
					<th>Password</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
			<?php
				$no = 1;
				foreach ($user as $u) {
					echo '
						<tr>
							<td>'.$no.'</td>
							<td>'.$u->id_user.'</td>
							<td>'.$u->nama.'</td>
							<td>'.$u->username.'</td>
							<td>'.$u->password.'</td>
							<td>
								<a href="#" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ubah" onclick="prepare_ubah_user('.$u->id_user.')">Ubah</a>
								<a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus" onclick="prepare_hapus_user('.$u->id_user.')">hapus</a>
							</td>
						</tr>
					';
					$no++;
				}
			?>
				
			</tbody>
		</table>
<!-- END TABLE STRIPED -->
	</div>
</div>
<!-- Modal -->
<div id="tambah" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Buku</h4>
      </div>
      <form action="<?php echo base_url('index.php/user/tambah'); ?>" method="post" enctype="multipart/form-data">
	      <div class="modal-body">
	        	<input type="text" class="form-control" placeholder="Nama" name="nama">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Username" name="username">
	        	<br>
	        	<input type="password" class="form-control" placeholder="Password" name="password">
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" name="submit" value="SIMPAN">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      </form>
    </div>
  </div>
</div>
<div id="ubah" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah menu</h4>
      </div>
      <form action="<?php echo base_url('index.php/user/ubah'); ?>" method="post" enctype="multipart/form-data">
	      <div class="modal-body">
	        	<input type="hidden" name="ubah_id_user"  id="ubah_id_user">
	        	<input type="text" class="form-control" placeholder="Id User" name="ubah_kode_user"  id="ubah_kode_user">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Nama" name="ubah_nama"  id="ubah_nama">
	        	<br>
	        	<input type="text" class="form-control" placeholder="Username" name="ubah_username"  id="ubah_username">
	        	<br>
	        	<input type="password" class="form-control" placeholder="Password" name="ubah_password" id="ubah_password">
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-primary" name="submit" value="SIMPAN">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      </form>
    </div>
  </div>
</div>
<div id="hapus" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi Hapus Data Buku</h4>
      </div>
      <form action="<?php echo base_url('index.php/user/hapus'); ?>" method="post">
	      <div class="modal-body">
	        	<input type="hidden" name="hapus_id_user"  id="hapus_id_user">
	        	<p>Apakah anda yakin menghapus data user <b><span id="hapus_judul"></span></b> ?</p>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-danger" name="submit" value="YA">
	        <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
	      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
	
	function prepare_ubah_user(id)
	{
		$("#ubah_id_user").empty();
		$("#ubah_kode_user").empty();
		$("#ubah_nama").empty();;
		$("#ubah_username").empty();
		$("#ubah_password").empty();
		

		$.getJSON('<?php echo base_url(); ?>index.php/user/get_data_user_by_id/' + id,  function(data){
			$("#ubah_id_user").val(data.id_user);
			$("#ubah_kode_user").val(data.id_user);
			$("#ubah_nama").val(data.nama);
			$("#ubah_username").val(data.username);
			$("#ubah_password").val(data.password);
			

		});
	}

	function prepare_hapus_user(id)
	{
		$("#hapus_id_user").empty();
		$("#hapus_judul").empty();

		$.getJSON('<?php echo base_url(); ?>index.php/user/get_data_user_by_id/' + id,  function(data){
			$("#hapus_id_user").val(data.id_user);
			$("#hapus_judul").text(data.username);
		});
	}
</script>