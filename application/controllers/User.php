<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data['main_view'] = 'user_view';
			$data['user'] = $this->user_model->get_user();
			$this->load->view('template', $data);

		} else {
			redirect('login/index');
		}
	}
	public function tambah()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				if($this->user_model->tambah() == TRUE)
				{
					$this->session->set_flashdata('notif', 'Tambah User berhasil');
					redirect('user/index');
				} else {
					$this->session->set_flashdata('notif', 'Tambah User gagal');
					redirect('user/index');
				}
			} else {
				$this->session->set_flashdata('notif', validation_errors());
				redirect('user/index');
			}


		} else {
			redirect('login/index');
		}
	}
	public function ubah()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$this->form_validation->set_rules('ubah_nama', 'Nama', 'trim|required');
			$this->form_validation->set_rules('ubah_username', 'username', 'trim|required');
			$this->form_validation->set_rules('ubah_password', 'password', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				if($this->user_model->ubah() == TRUE)
				{
					$this->session->set_flashdata('notif', 'Ubah user berhasil');
					redirect('user/index');
				} else {
					$this->session->set_flashdata('notif', 'Ubah user gagal');
					redirect('user/index');
				}
			} else {
				$this->session->set_flashdata('notif', validation_errors());
				redirect('user/index');
			}


		} else {
			redirect('login/index');
		}
	}
	public function get_data_user_by_id($id)
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data = $this->user_model->get_data_user_by_id($id);
			echo json_encode($data);

		} else {
			redirect('login/index');
		}
	}
	public function hapus()
	{
		if($this->session->userdata('logged_in') == TRUE){

			if($this->user_model->hapus() == TRUE){
				$this->session->set_flashdata('notif', 'Hapus user Berhasil');
				redirect('user/index');
			} else {
				$this->session->set_flashdata('notif', 'Hapus user gagal');
				redirect('user/index');
			}

		} else {
			redirect('login/index');
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */