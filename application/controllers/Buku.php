<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('buku_model');
	}
	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data['main_view'] = 'buku_view';
			$data['buku'] = $this->buku_model->get_buku();

			$this->load->view('template', $data);

		} else {
			redirect('login/index');
		}
	}
	public function tambah()
	{
		if($this->session->userdata('logged_in') == TRUE)
		{
			$this->form_validation->set_rules('buku', 'judul_buku', 'trim|required');
			$this->form_validation->set_rules('penerbit', 'penerbit', 'trim|required');
			$this->form_validation->set_rules('tahun', 'tahun', 'trim|required');
			$this->form_validation->set_rules('harga', 'harga', 'trim|required|numeric');
			$this->form_validation->set_rules('stok', 'stok', 'trim|required|numeric');	

			if ($this->form_validation->run() == TRUE) {
				if($this->buku_model->tambah() == TRUE){
					$this->session->set_flashdata('notif', 'Tambah Buku berhasil');
									redirect('buku/index');
				} else{
					$this->session->set_flashdata('notif', 'Tambah Buku Gagal');
					redirect('buku/index');
				}
				
				} else {
					$this->session->set_flashdata('notif', 'Tambah Buku Gagal');
					redirect('buku/index');
				}

		} else {
			redirect('login/index');
		}
	}
	public function get_data_buku_by_id($id)
	{
		if($this->session->userdata('logged_in') == TRUE){

			$data = $this->buku_model->get_data_buku_by_id($id);
			echo json_encode($data);

		} else {
			redirect('login/index');
		}
	}
	public function ubah()
	{
		if($this->session->userdata('logged_in') == TRUE){

			$this->form_validation->set_rules('ubah_buku', 'judul_buku', 'trim|required');
			$this->form_validation->set_rules('ubah_penerbit', 'penerbit', 'trim|required');
			$this->form_validation->set_rules('ubah_tahun', 'tahun', 'trim|required|numeric');
			$this->form_validation->set_rules('ubah_harga', 'harga', 'trim|required|numeric');
			$this->form_validation->set_rules('ubah_stok', 'harga', 'trim|required|numeric');

			if ($this->form_validation->run() == TRUE) {
				if($this->buku_model->ubah() == TRUE)
				{
					$this->session->set_flashdata('notif', 'Ubah Data Buku berhasil');
					redirect('buku/index');
				} else {
					$this->session->set_flashdata('notif', 'Ubah Data gagal');
					redirect('buku/index');
				}
			} else {
				$this->session->set_flashdata('notif', validation_errors());
				redirect('buku/index');
			}


		} else {
			redirect('login/index');
		}
	}
	public function hapus()
	{
		if($this->session->userdata('logged_in') == TRUE){

			if($this->buku_model->hapus() == TRUE){
				$this->session->set_flashdata('notif', 'Hapus Data Buku Berhasil');
				redirect('buku/index');
			} else {
				$this->session->set_flashdata('notif', 'Hapus Data Buku gagal');
				redirect('buku/index');
			}

		} else {
			redirect('login/index');
		}
	}

}

/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */