<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
		{
			parent::__construct();
			$this->load->model('register_model');
		}
	public function index()
	{
		$this->load->view('register_view');		
	}
	public function tambah()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		
		if($this->form_validation->run() == TRUE) {
			if($this->register_model->tambah() == TRUE)
			{
				$this->session->set_flashdata('notif', 'Berhasil Mendaftar');
				redirect('register/index');
			} else {
				$this->session->set_flashdata('notif', 'Gagal Mendaftar');
				redirect('login/index');
			}
		} else {
			redirect('login/index');
		}
	}

}

/* End of file Register.php */
/* Location: ./application/controllers/Register.php */